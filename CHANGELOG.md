# Changelog
All notable changes to this library will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### NEW
#### General
- Schema support with validation via `validation` option
### FIXES
#### Collection Methods
- user can pass custom `_key` item and it not be overwritten using uuid
### CHANGES - BREAKING
#### Collection Methods
- all write operations now have a standard return object `WriteOpsResult(s)` user can find the data transfered in the `data` property of the response.

[comment]: <> (######################################################)
[comment]: <> (############ Version 0.1.1 Release ###################)
[comment]: <> (######################################################)
## [0.1.1] - 2018-03-28
[comment]: <> (### CHANGES - BREAKING)
### NEW
#### General
- Changelog
- TSLint
- TSConfig
- Logger
#### Database Methods
- SQL Database methods and interface
  * `connect:` create connection to db (```connect (): Promise<Databases>```)
  * `closeConnection:` close connection to db (```closeConnection (): Promise<void>```)
  * `createUniqueIndex:` create unique index on the collection table given (```createUniqueIndex (column: string, indexname?: string): void```)
  * `query:` user can inject custom query with params to execute. (```query (query : string , params : any[]) : Promise<any>```)
- Methods tested:
  * Create connection
  * Create collection
  * Create index
  * Disconnection connection

#### Collection Methods
- SQL Collection methods and interface
  * `create:` insert items into collection (```create(...model: Model[]): Promise<Model[]>```)
  * `duplicateById:` create a new item from an already existing one (```duplicateById(id: string): Promise<Model|Model[]>```)
  * `findById:` find an item by id (```findById(id: string): Promise<Model | null>```)
  * `findOne:` find one item with filter (```findOne(filter: Filter): Promise<Model | null>```)
  * `find:` find any item with filter (```find(filter: Filter): Promise<Model[] | null>```)
  * `updateById:` update an item by id (```updateById(id: string, model: any): Promise<UpdateWriteOpResult>```)
  * `update:` update any item by filter (```update(filter: any, model: any): Promise<UpdateWriteOpResult>```)
  * `removeById:` remove an item by id (```removeById(id: string): Promise<DeleteWriteOpResultObject>```)
  * `remove:` remove any item by filter (```remove(filter: Filter): Promise<DeleteWriteOpResultObject>```)
- Methods tested:
  * Import
  * Duplicate item
  * Find id
  * Find single
  * Find any
  * Update id
  * Update any
  * Remove id
  * Remove any