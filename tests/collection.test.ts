import { ArangoCollection, CollectionFacadeOptions } from '../src';
import { Database } from 'arangojs';
import { log } from '../src/log';
import chai = require('chai');

import dotenv = require('dotenv');
const process = dotenv.config()

interface Model {
	_key?: string
	_id?: string
	string?: string
}

let facade: ArangoCollection<Model>
let db: Database
let testid: string
let teststring: string
const options: CollectionFacadeOptions = {
	schema: {
		_id: {name: '_id', dataType: String},
		string: {name: 'string', dataType: String},
	},
	collection: 'CollectionTest',
	connectionUrl: String(process.parsed.CONNECTION_STRING),
	logLevel: 'warn',
	validation: true
}

describe('Arangodb collection class test', function(): void {
	before( function(done: MochaDone): void {
		facade = new ArangoCollection(options)
		facade.connect()
			.then((database) => {
				db = database
				log.info('Connected')
				done()})
			.catch((err) => {done(err)})
	})

	after(function(done: MochaDone): void {
		facade.dropCollection()
		.then(() => {
			log.info('Collection dropped')
			facade.closeConnection()
				.then(() => {log.info('connection closed'); done()})
				.catch((err) => {done(err)})
		})
		.catch(done)
	})

	describe('Create', function(): void {
		it('should create test element', function(done: MochaDone): void {
			facade.create({ string: 'one'})
				.then((model) => {
					chai.expect(model.data[0]._key).not.to.equal(undefined)
					testid = model.data[0]._key as string
					teststring = model.data[0].string as string
					done()
				}).catch(done)
		})
		it('should create a duplicate test element from id', function(done: MochaDone): void {
			facade.duplicateById(testid)
				.then((model) => {
					chai.expect(model.data._key).not.to.equal(testid)
					chai.expect(model.data.string).to.equal(teststring)
					done()
				}).catch(done)
		})
		it('should create several test elements', function(done: MochaDone): void {
			const multiple = [{string: 'two'}, {string: 'three'}, {string: 'four'}]
			facade.create(...multiple)
				.then((models) => {
					chai.expect(models.data).length(3)
					models.data.forEach((model) => {
						chai.expect(model._key).not.to.equal(undefined)
					})
					done()
				}).catch(done)
		})
	})

	describe('Find', function(): void {
		it('should find all elements in collection', function(done: MochaDone): void {
			facade.find({})
				.then((res) => {
					chai.expect(res).length.to.be.greaterThan(3)
					done()
				})
				.catch(done)
		})
		it('should find single element where string $eq "three"', function(done: MochaDone): void {
			facade.find({ string: { $eq: 'three'}})
				.then((res) => {
					chai.expect(res).length(1)
					chai.expect(res[0].string).to.equal('three')
					done()
				})
				.catch(done)
		})
		it('should find single element where string $eq "two" $or string $eq "three"', function(done: MochaDone): void {
			facade.find({ $or: [{ string: {$eq: 'two'}}, {string: {$eq: 'three'}}]})
				.then((res) => {
					chai.expect(res).length(2)
					done()
				})
				.catch(done)
		})
		it('should findOne element', function(done: MochaDone): void {
			facade.findOne({})
				.then((res) => {
					chai.expect(res).to.be.an('object')
					done()
				})
				.catch(done)
		})
		it('should findOne element where string $eq "one"', function(done: MochaDone): void {
			facade.findOne({ string: { $eq: 'one' } })
				.then((res) => {
					chai.expect(res).to.be.an('object')
					chai.expect(res.string).to.equal('one')
					done()
				})
				.catch(done)
		})
		it('should find no element', function(done: MochaDone): void {
			facade.findOne({ string: { $eq: 'ninetynine' } })
				.then((res) => {
					chai.expect(res).to.be.an('object')
					chai.expect(res).to.be.empty
					done()
				})
				.catch(done)
		})
		it('should find element where string $eq "one" by id', function(done: MochaDone): void {
			facade.findById(testid)
				.then((res) => {
					chai.expect(res._key as string).to.be.equal(testid)
					chai.expect(res.string as string).to.be.equal(teststring)
					done()
				})
				.catch(done)
		})
		it('should find no element by id', function(done: MochaDone): void {
			facade.findById('999999999999999999999999')
				.then((res) => {
					chai.expect(res).to.be.an('object')
					chai.expect(res).to.be.empty
					done()
				})
				.catch(done)
		})
	})

	describe('Update', function(): void {
		it('should update all elements with string "updatedOne"', function(done: MochaDone): void {
			const newUpdate: string = 'updatedOne'
			facade.update({} , {$set: {string: newUpdate }} )
			.then(async () => {
				try {
					const docs = await facade.find({string: {$eq: newUpdate }})
					chai.expect(docs).length.to.be.gt(3)
					done()
				} catch (err) {done(err)}
			})
			.catch(done)
		})
		it('should update single test element by Id with string "updatedById"', function(done: MochaDone): void {
			const newIdUpdate = 'updatedById'
			facade.updateById(testid , {$set: { string: newIdUpdate }})
			.then(async () => {
				try {
					const docs = await facade.find({string: {$eq: newIdUpdate}})
					chai.expect(docs.length).to.equal(1)
					done()
				} catch (err) {done(err)}
			})
			.catch(done)
		})
	})

	describe('Custom Query', function(): void {
		it('should should return a list of all items created in collection', function(done: MochaDone): void {
			facade.query(`FOR u in CollectionTest RETURN u`)
			.then((res) => {
				chai.expect(res).to.be.an('object')
				chai.expect(res.data.length).to.be.greaterThan(3)
				done()
			})
			.catch(done)
		})
		it('should should return a list of items created in collection where string is equal to "updatedById"', function(done: MochaDone): void {
			facade.query(`FOR u in CollectionTest FILTER (u.string == 'updatedById') RETURN u`)
			.then((res) => {
				chai.expect(res).to.be.an('object')
				chai.expect(res.data.length).to.be.equal(1)
				done()
			})
			.catch(done)
		})
	})

	// describe('Index', function(): void {
	// 	it('should should create a unique index on the _id column', function(done: MochaDone): void {
	// 		facade.createUniqueIndex('_id')
	// 			.then(async () => {
	// 				await db.describe({table: 'CollectionTest', type: 'index', column: '_id', database: String(process.parsed.DATABASE)}, (err: Error, result) => {
	// 					if (err) {done(err)}
	// 					chai.expect(result).to.be.an('array')
	// 					chai.expect(result.length).to.be.equal(1)
	// 					done()
	// 				})
	// 			})
	// 			.catch(done)
	// 	})
	// })

	describe('Remove', function(): void {
		it('should remove test element by id', function(done: MochaDone): void {
			facade.removeById(testid)
				.then(async () => {
					try {
						const doc = await facade.findById(testid)
						chai.expect(doc).to.be.an('object')
						chai.expect(doc).to.be.empty
						done()
					} catch (err) {done(err)}
				})
				.catch(done)
		})
		it('should remove all elements', function(done: MochaDone): void {
			facade.remove({})
				.then(async () => {
					try {
						const docs = await facade.find({})
						chai.expect(docs.length).to.equal(0)
						done()
					} catch (err) {done(err)}
				})
				.catch(done)
		})
	})

})