import { ArangoEdge, EdgeFacadeOptions } from '../src';
import { Database } from 'arangojs';
import { log } from '../src/log';
import chai = require('chai');

import dotenv = require('dotenv');
const process = dotenv.config()

interface Model {
	_key?: string
	_to?: string
	_from?: string
	label?: string
}

let facade: ArangoEdge<Model>
let db: Database
let testid: string
let teststring: string
const options: EdgeFacadeOptions = {
	schema: {
		_key: {name: '_id', dataType: String},
		_to: {name: 'to', dataType: String},
		_from: {name: 'from', dataType: String},
		label: {name: 'label', dataType: String}
	},
	edge: 'EdgeTest',
	connectionUrl: String(process.parsed.CONNECTION_STRING),
	logLevel: 'warn',
	validation: true
}

describe('Arangodb Edge class test', function(): void {
	before( function(done: MochaDone): void {
		facade = new ArangoEdge(options)
		facade.connect()
		.then((database) => {
			db = database
			log.info('Connected')
			done()})
		.catch((err) => {done(err)})
	})

	after(function(done: MochaDone): void {
		facade.dropEdge()
		.then(() => {
			log.info('Edge dropped')
			facade.closeConnection()
			.then(() => {log.info('connection closed'); done()})
			.catch((err) => {done(err)})
		})
		.catch(done)
	})

	describe('Create', function(): void {
		it('should create test element', function(done: MochaDone): void {
			facade.create({_from: 'cape/cape', _to: 'cario/cairo', label: 'morroco'})
				.then((model) => {
					chai.expect(model.data[0]._key).not.to.equal(undefined)
					testid = model.data[0]._key as string
					teststring = model.data[0].label as string
					done()
				}).catch(done)
		})
		it('should create several test elements', function(done: MochaDone): void {
			const multiple = [{_from: 'cape/cape', _to: 'cario/cairo', label: 'madagascar'}, {_from: 'cape/cape', _to: 'cario/cairo', label: 'azania'} ]
			facade.create(...multiple)
				.then((models) => {
					chai.expect(models.data).length(2)
					models.data.forEach((model) => {
						chai.expect(model._key).not.to.equal(undefined)
					})
					done()
				}).catch(done)
		})
	})

	describe('Find', function(): void {
		it('should find all elements in Edge', function(done: MochaDone): void {
			facade.find({})
				.then((res) => {
					chai.expect(res).length.to.be.greaterThan(2)
					done()
				})
				.catch(done)
		})
		it('should find single element where string $eq "azania"', function(done: MochaDone): void {
			facade.find({ label: { $eq: 'azania'}})
				.then((res) => {
					chai.expect(res).length(1)
					chai.expect(res[0].label).to.equal('azania')
					done()
				})
				.catch(done)
		})
		it('should find single element where string $eq "azania" $or string $eq "madagascar"', function(done: MochaDone): void {
			facade.find({ $or: [{ label: {$eq: 'azania'}}, {label: {$eq: 'madagascar'}}]})
				.then((res) => {
					chai.expect(res).length(2)
					done()
				})
				.catch(done)
		})
		it('should findOne element', function(done: MochaDone): void {
			facade.findOne({})
				.then((res) => {
					chai.expect(res).to.be.an('object')
					done()
				})
				.catch(done)
		})
		it('should findOne element where string $eq "one"', function(done: MochaDone): void {
			facade.findOne({ label: { $eq: 'madagascar' } })
				.then((res) => {
					chai.expect(res).to.be.an('object')
					chai.expect(res.label).to.equal('madagascar')
					done()
				})
				.catch(done)
		})
		it('should find no element', function(done: MochaDone): void {
			facade.findOne({ label: { $eq: 'bazooka' } })
				.then((res) => {
					chai.expect(res).to.be.an('object')
					chai.expect(res).to.be.empty
					done()
				})
				.catch(done)
		})
		it('should find element where string $eq "one" by id', function(done: MochaDone): void {
			facade.findById(testid)
				.then((res) => {
					chai.expect(res._key as string).to.be.equal(testid)
					chai.expect(res.label as string).to.be.equal(teststring)
					done()
				})
				.catch(done)
		})
		it('should find no element by id', function(done: MochaDone): void {
			facade.findById('999999999999999999999999')
				.then((res) => {
					chai.expect(res).to.be.an('object')
					chai.expect(res).to.be.empty
					done()
				})
				.catch(done)
		})
	})

	describe('Update', function(): void {
		it('should update all elements with string "freedomCharter"', function(done: MochaDone): void {
			const newUpdate: string = 'freedomCharter'
			facade.update({} , {$set: {label: newUpdate }} )
			.then(async () => {
				try {
					const docs = await facade.find({label: {$eq: newUpdate }})
					chai.expect(docs).length.to.be.gt(2)
					done()
				} catch (err) {done(err)}
			})
			.catch(done)
		})
		it('should update single test element by Id with string "Makoroba"', function(done: MochaDone): void {
			const newIdUpdate = 'Makoroba'
			facade.updateById(testid , {$set: { label: newIdUpdate }})
			.then(async () => {
				try {
					const docs = await facade.find({label: {$eq: newIdUpdate}})
					chai.expect(docs.length).to.equal(1)
					done()
				} catch (err) {done(err)}
			})
			.catch(done)
		})
	})

	describe('Custom Query', function(): void {
		it('should should return a list of all items created in Edge', function(done: MochaDone): void {
			facade.query(`FOR u in EdgeTest RETURN u`)
			.then((res) => {
				chai.expect(res).to.be.an('object')
				chai.expect(res.data.length).to.be.greaterThan(2)
				done()
			})
			.catch(done)
		})
		it('should should return a list of items created in Edge where string is equal to "Makoroba"', function(done: MochaDone): void {
			facade.query(`FOR u in EdgeTest FILTER (u.label == 'Makoroba') RETURN u`)
			.then((res) => {
				chai.expect(res).to.be.an('object')
				chai.expect(res.data.length).to.be.equal(1)
				done()
			})
			.catch(done)
		})
	})

	// describe('Index', function(): void {
	// 	it('should should create a unique index on the _id column', function(done: MochaDone): void {
	// 		facade.createUniqueIndex('_id')
	// 			.then(async () => {
	// 				await db.describe({table: 'UniTests', type: 'index', column: '_id', database: String(process.parsed.DATABASE)}, (err: Error, result) => {
	// 					if (err) {done(err)}
	// 					chai.expect(result).to.be.an('array')
	// 					chai.expect(result.length).to.be.equal(1)
	// 					done()
	// 				})
	// 			})
	// 			.catch(done)
	// 	})
	// })

	describe('Remove', function(): void {
		it('should remove test element by id', function(done: MochaDone): void {
			facade.removeById(testid)
				.then(async () => {
					try {
						const doc = await facade.findById(testid)
						chai.expect(doc).to.be.an('object')
						chai.expect(doc).to.be.empty
						done()
					} catch (err) {done(err)}
				})
				.catch(done)
		})
		it('should remove all elements', function(done: MochaDone): void {
			facade.remove({})
				.then(async () => {
					try {
						const docs = await facade.find({})
						chai.expect(docs.length).to.equal(0)
						done()
					} catch (err) {done(err)}
				})
				.catch(done)
		})
	})

})