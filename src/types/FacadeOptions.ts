import { Modelschema } from '@simplus/si-simple-schema';

export interface DatabaseStorageFacadeOptions {
	/**Connection Url
     *
     * Consider the following formats:
     * ODBC => DRIVER={FreeTDS};SERVER=host;UID=user;PWD=password;DATABASE=dbname
     */
	connectionUrl: string
	/**Log level
     *
     * Set the minimum level at which you want logging to occur.
     */
	logLevel?: 'fatal' | 'error' | 'warn' | 'info' | 'debug' | 'trace'
}


export interface CollectionFacadeOptions extends DatabaseStorageFacadeOptions {
	/**ModelSchema
     *
     * Definition of storage rule for validation.
     *
    */
	schema: Modelschema
	/**Collection name
     *
     *Name of collection storage
     */
	collection: string
	/**Validation rules
     *
     *Activate schema validation for create method
     */
	validation?: boolean
}

export interface EdgeFacadeOptions extends DatabaseStorageFacadeOptions {
	/**ModelSchema
     *
     * Definition of storage rule for validation.
     *
    */
	schema: Modelschema
	/**Collection name
     *
     *Name of edge storage
     */
	edge: string
	/**Validation rules
     *
     *Activate schema validation for create method
     */
	validation?: boolean
}
