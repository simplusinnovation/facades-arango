export { CollectionFacade  } from './CollectionFacade';
export { EdgeFacade  } from './EdgeFacade';
export { EdgeFacadeOptions, CollectionFacadeOptions, DatabaseStorageFacadeOptions } from './FacadeOptions';
export { DatabaseStorageFacade } from './DatabaseStorageFacade';
