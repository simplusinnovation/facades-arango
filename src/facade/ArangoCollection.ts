//////////////////////////////////////////////////////////////////////////////////////////////////
// ArangoCollection Interface requirements
//////////////////////////////////////////////////////////////////////////////////////////////////
import { CollectionFacade, CollectionFacadeOptions } from '../types';
import { WriteOpsResult, WriteOpsResults, WriteOpsStats } from '../types/CollectionFacade';
import { SimpleSchema, ValidationOutput } from '@simplus/si-simple-schema';
import { DocumentCollection } from 'arangojs/lib/cjs/collection';
import { ArangoError } from 'arangojs/lib/cjs/error';
import { Filter, Update, chirp} from '@simplus/si-query-object';
import { ArangoDatabaseStorage } from './ArangoDatabaseStorage';
import { ArrayCursor } from 'arangojs/lib/cjs/cursor';
import { omit, last, defaults } from 'lodash';
import { ValidationError } from 'joi';
import { Database } from 'arangojs';
import { v4 } from 'uuid';
import qb = require('aqb');

//////////////////////////////////////////////////////////////////////////////////////////////////
// CollectionFacade Interface requirements
//////////////////////////////////////////////////////////////////////////////////////////////////
export class ArangoCollection<Model extends {_key?: string}> extends ArangoDatabaseStorage implements CollectionFacade<Model> {
	private _options: CollectionFacadeOptions
	private _schema: SimpleSchema
	private _collection: DocumentCollection | undefined

	constructor(options: CollectionFacadeOptions) {
		super(options);
		this._options = options
		this._collection = undefined
		this._schema = new SimpleSchema(defaults({_key: {dataType: String, primaryKey: true, name: '_key'}}, options.schema))
		this._loglevel = options ? (options.logLevel ? options.logLevel : 'info') : 'info'
		this._log.level(this._loglevel)
	}
	/**Connect
	 *
	 * Creates a connection to the database
	 *
	 * @example
	 * facade.connect().then((database) => {database.query()}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	connect():  Promise<Database> {
		return new Promise((resolve, reject) => {
			super.connect(this._options.connectionUrl).then(async (db: Database) => {
				// First check connection to database
				if (!this._db) {
					this._db = db
				}
				// First check connection to collection
				if (!this._collection) {
					try {
						// check if the collection has already been created
						const collections = await this._db.collections(true)
						const collection = collections.find((c: {name: string}) => c.name === this._options.collection)
						this._collection = db.collection(this._options.collection)

						// if the collection does not already exist, then create it
						if (!collection) {
							await this._collection.create({'waitForSync': true})
							await this._collection.load(false)
						}
						this._log.info(new Date(), 'collection created => : ', this._options.collection);
						resolve(db)
					} catch (err) {this._log.error(new Date(), ' : ', err) ; reject(err); }
				}
				// collection
				if (this._collection) {
					resolve(db)
				}
			}).catch((err) => {this._log.error(new Date(), ' : ', err); reject(err)})
		})
	}
	/**dropCollection
	 *
	 * Drops the collection.
	 *
	 * @example
	 * facade.dropCollection().then(() => {console.log('dropped collection')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	dropCollection(): Promise<void> {
		return new Promise((resolve, reject) => {
			if (this._collection) {
				this._collection.drop()
				.then(() => {
					this._log.info(new Date(), ' : dropped collection  => : ', this._options.collection, '\n')
					resolve()
				})
				.catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			} else { this._log.error(new Date(), ' : No collection table to drop\n'); reject('No collection table to drop')}
		})
	}
	/**Create
	 *
	 * inserts data into database
	 *
	 * @param model		:model object to insert as array
	 *
	 * @example
	 * facade.create({name: Athenkosi, surname: Mase}).then((data) => {console.log('model inserted')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	create(...model: Model[]): Promise<WriteOpsResults<Model>> {
		return new Promise((resolve, reject) => {
			// If user supplies the `_key` item then we do not overwrite it with uuid.
			model.map((m: {}) => {defaults(m, {_key: v4()})})
			const validation = (this._options.validation) ? this._schema.validate(...model) : {status: true, content: model, error: null} as ValidationOutput<Model>
			if (!validation.status) {
				this._log.error(new Date(), ' : ', validation.error)
				reject((validation.error as ValidationError).details)
			}
			if (this._collection) {
				this._collection.import(validation.content, {waitForSync: true, onDuplicate: 'update'})
				.then((res: WriteOpsStats) => {
					this._log.info(new Date(), ' : ', res)
					resolve({stats: res, data: model})
				})
				.catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			} else { this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')}
		})
	}
	/**DuplicateById
	 *
	 * creates a duplicate entry with a new unique id.
	 *
	 * @param id		:unique id of element to duplicate
	 *
	 * @example
	 * facade.duplicateById('12345').then((data) => {console.log('model duplicated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	duplicateById(id: string): Promise<WriteOpsResult<Model>> {
		return new Promise((resolve, reject) => {
			this.findById(id)
			.then((model) => {
				this.create(omit(model, ['_id', '_v', '_key', '_rev']))
				.then((data) => {resolve({stats: data.stats, data: data.data[0]} as WriteOpsResult<Model>)})
				.catch(reject)
			}).catch(reject)
		})
	}
	/**Update
	 *
	 * update element(s) with new model information based on filter.
	 *
	 * @param filter		:filter parameters to segment items
	 * @param model			:rule to update model
	 *
	 * @example
	 * facade.update({name: {$eq : 'Athenkosi'}}, {$set :{name: 'Yehudi'}}).then((data) => {console.log('name updated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	update(filter: Filter, update: Update): Promise<WriteOpsResults<Model>> {
		return new Promise((resolve, reject) => {
			if (this._collection && this._db) {
				const pref: string = 'u'
				const F = chirp.Filter.Arango(filter, {prefix: pref});
				const U = chirp.Update.Arango(update, {prefix: pref});

				const query = qb.for(pref).in(this._options.collection)
				.filter(F ? F : [])
				.update(pref).with(U ? U : {}).in(this._options.collection)
				.options({ keepNull: false })

				this._db.query(query, {}, {profile: true})
				.then(async (cursor: ArrayCursor) => {
					return {stats: {
							error: false
							, created: 0
							, errors: cursor.extra.warnings.length
							, updated: cursor.extra.stats.writesExecuted
							, ignored: cursor.extra.stats.writesIgnored
						} , data: await cursor.all()}
				})
				.then((res: WriteOpsResults<Model>) => {
					this._log.info(new Date(), ' : ', res.stats , '\n')
					resolve(res)
				})
				.catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			}
			if (!this._collection) {this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')} else if (!this._db) {this._log.error(new Date() , ' : Database not connected\n'); reject('Database not connected')}
		})
	}
	/**Update By Id
	 *
	 * update element with new model information based on id.
	 *
	 * @param id			:element unique id
	 * @param model			:rule to update model
	 *
	 * @example
	 * facade.updateById('12345', {$set :{name: 'Yehudi'}}).then((data) => {console.log('name updated')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	updateById(id: string, update: Update): Promise<WriteOpsResult<Model>> {
		return new Promise((resolve, reject) => {
			if (this._collection && this._db) {
				const pref: string = 'u'
				const F = chirp.Filter.Arango({_key: {$eq : id}}, {prefix: pref});
				const U = chirp.Update.Arango(update, {prefix: pref});

				const query = qb.for(pref).in(this._options.collection)
				.filter(F ? F : [])
				.update(pref).with(U ? U : {}).in(this._options.collection)
				.options({ keepNull: false })

				this._db.query(query)
				.then(async (cursor: ArrayCursor) => {
					return {stats: {
						error: false
						, created: 0
						, errors: cursor.extra.warnings.length
						, updated: cursor.extra.stats.writesExecuted
						, ignored: cursor.extra.stats.writesIgnored
					} , data: await cursor.all()}
				})
				.then((res: WriteOpsResult<Model>) => {
					this._log.info(new Date(), ' : ', res.stats , '\n')
					resolve(res)
				})
				.catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			}
			if (!this._collection) {this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')} else if (!this._db) {this._log.error(new Date() , ' : Database not connected\n'); reject('Database not connected')}
		})
	}
	/**Find One
	 *
	 * find first element to appear based on filter
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.findOne({name: {$eq : 'Athenkosi'}}).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	findOne(filter: Filter): Promise<Model> {
		return new Promise((resolve, reject) => {
			if (this._collection && this._db) {
				const pref: string = 'u'
				const F = chirp.Filter.Arango(filter, {prefix: pref})
				const query = qb.for(pref).in(this._options.collection)
				.filter(F)
				.limit(1)
				.return(pref)

				this._db.query(query)
				.then((cursor: ArrayCursor) => {
					return cursor.all()
				})
				.then((res: Model[]) => {
					this._log.info(new Date(), ' : ', res[0] || {} , '\n')
					resolve(res[0] || {})
				}).catch((err: ArangoError) => {
					if (err.code === 404) {this._log.info(new Date(), ' : }} \n'); resolve({} as Model)} else {this._log.error(new Date(), ' : ', err); reject(err)}
				})
			}
			if (!this._collection) {this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')} else if (!this._db) {this._log.error(new Date() , ' : Database not connected\n'); reject('Database not connected')}
		})
	}
	/**Find By Id
	 *
	 * find element based on the unique id
	 *
	 * @param id		:unique identifier string
	 *
	 * @example
	 * facade.findById('1234').then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	findById(id: string): Promise<Model> {
		return new Promise((resolve, reject) => {
			if (this._collection && this._db) {
				this._collection.document(id)
				.then((res: Model) => {
					this._log.info(new Date(), ' : ', res , '\n')
					resolve(res)
				}).catch((err: ArangoError) => {
					if (err.code === 404) {this._log.info(new Date(), ' : }} \n'); resolve({} as Model)} else {this._log.error(new Date(), ' : ', err); reject(err)}
				})
			}
			if (!this._collection) {this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')} else if (!this._db) {this._log.error(new Date() , ' : Database not connected\n'); reject('Database not connected')}
		})
	}
	/**Find any
	 *
	 * find any or all elements in collection
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.findById({month: {$eq: 'January'}}).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	find(filter: Filter): Promise<Model[]> {
		return new Promise((resolve, reject) => {
			if (this._collection && this._db) {
				const F = chirp.Filter.Arango(filter, {prefix: 'u'})
				const query = qb.for('u').in(this._options.collection)
				.filter(F)
				.return('u')

				this._db.query(query.toAQL())
				.then((cursor: ArrayCursor) => {
					return cursor.all()
				})
				.then((docs: Model[]) => {
					this._log.info(new Date(), ' : ', docs , '\n')
					resolve(docs)
				})
				.catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			}
			if (!this._collection) {this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')} else if (!this._db) {this._log.error(new Date() , ' : Database not connected\n'); reject('Database not connected')}
		})
	}
	/**Remove any
	 *
	 * delete any or all elements in collection
	 *
	 * @param filter		:filter parameters to segment items
	 *
	 * @example
	 * facade.remove({month: {$eq: 'January'}}).then((res) => {console.log('items removed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	remove(filter: Filter): Promise<WriteOpsStats> {
		return new Promise((resolve, reject) => {
			if (this._collection && this._db) {
				const F = chirp.Filter.Arango(filter)
				const query = qb.for('u').in(this._options.collection)
				.filter(F ? F : [])
				.remove('u').in(this._options.collection)

				this._db.query(query.toAQL())
				.then(async (cursor: ArrayCursor) => {
					return {
						error: false
						, created: 0
						, errors: cursor.extra.warnings.length
						, removed: cursor.extra.stats.writesExecuted
						, ignored: cursor.extra.stats.writesIgnored
					}
				})
				.then((res: WriteOpsStats) => {
					this._log.info(new Date(), ' : ', res , '\n')
					resolve(res)
				}).catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			}
			if (!this._collection) {this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')} else if (!this._db) {this._log.error(new Date() , ' : Database not connected\n'); reject('Database not connected')}
		})
	}
	/**Remove by id
	 *
	 * delete element based on the unique identifier
	 *
	 * @param id		:unique identifier string
	 *
	 * @example
	 * facade.removeById('1234').then((res) => {console.log('item removed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	removeById(id: string): Promise<WriteOpsStats> {
		return new Promise((resolve, reject) => {
			if (this._collection && this._db) {
				const key = last(id.split('/')) || ''

				this._collection.removeByKeys([key], {waitForSync: true})
				.then((res: WriteOpsStats) => {
					this._log.info(new Date(), ' : ', res , '\n')
					resolve(res)
				}).catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			}
			if (!this._collection) {this._log.error(new Date() , ' : No collection table to drop\n'); reject('No collection table to drop')} else if (!this._db) {this._log.error(new Date() , ' : Database not connected\n'); reject('Database not connected')}
		})
	}
	/**createUniqueIndex
	 *
	 * Creates a unique index on a column
	 *
	 * @param column	:unique index columns
	 *
	 * @example
	 * facade.closeConnection().then(() => {console.log('connection closed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns
	 */
	createUniqueIndex(...column: string[]): Promise<void> {
		return new Promise(async (resolve, reject) => {
			if (this._db && this._collection) {
				try {
					await this._collection.createHashIndex(column, {  'type' : 'hash', 'unique' : true, })
					this._log.info(new Date(), ' : Created Unique Index on column ', this._options.collection, '.', column)
					resolve()
				} catch (err) {; resolve()}
			} else {this._log.error(new Date(), ' : Database not connected'); reject('Database not connected')}
		})
	}
}

export default ArangoCollection