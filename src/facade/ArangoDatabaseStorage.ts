import { DatabaseStorageFacade, DatabaseStorageFacadeOptions } from '../types';
import { WriteOpsResults } from '../types/CollectionFacade';
import { ArrayCursor } from 'arangojs/lib/cjs/cursor';
import { ArangoError } from 'arangojs/lib/cjs/error';
import { last, take, pull } from 'lodash';
import { Database } from 'arangojs';
import bunyan = require('bunyan');
import { log } from '../log';
//////////////////////////////////////////////////////////////////////////////////////////////////
// CollectionFacade Interface requirements
//////////////////////////////////////////////////////////////////////////////////////////////////
export class ArangoDatabaseStorage implements DatabaseStorageFacade<Database> {
	protected _db: Database | null
	protected _log: bunyan
	protected _loglevel: DatabaseStorageFacadeOptions['logLevel']
	protected _connectionUrl: string | undefined

	constructor (options?: DatabaseStorageFacadeOptions) {
		this._db = null
		this._connectionUrl = (options && options.connectionUrl) ? options.connectionUrl : undefined
		this._loglevel = options ? (options.logLevel ? options.logLevel : 'info') : 'info'
		this._log = log
		this._log.level(this._loglevel)
	}
	/**Connect
	 *
	 * Creates a connection to the database
	 *
	 * @example
	 * facade.connect().then((database) => {database.query()}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<Database>
	 */
	connect(connectionUrl?: string): Promise<Database> {
		return new Promise((resolve, reject) => {
			const connection: string | undefined = connectionUrl || this._connectionUrl
			if (!connection) {
				this._log.error(`ConnectionUrl not defined`)
				reject(new Error(`ConnectionUrl not defined`))
			} else if (this._db) {
				resolve(this._db)
			} else {
				try {
					const connArray = connection.split('/')
					let database: string| undefined = undefined
					pull(connArray, '_db')
					if (connArray.length > 3) {database = last(connArray) as string}
					const Url = take(connArray, connArray.length - 1).join('/')
					this._db = new Database({'url': Url, 'arangoVersion': 30000})
					if (database) {this._db.useDatabase(database)}

					this._log.info(new Date(), ' : database connected')
					resolve(this._db)
				} catch (err) {this._log.error(new Date(), ' : ', err); reject(err)}
			}
		})
	}
	/**closeConnection
	 *
	 * Closes connection to the database
	 *
	 * @example
	 * facade.closeConnection().then(() => {console.log('connection closed')}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<void>
	 */
	closeConnection(): Promise<void> {
		return new Promise((resolve, reject) => {
			if (this._db) {
				try {
					// There currently is no Db disconnect method in Arango Js
					// So we simply nullify the _db constructor
					this._db = null
					resolve()
				} catch (err) {this._log.error(new Date(), ' : ', err); reject(err)}
			} else {this._log.warn(new Date(), ' : database connection closed - no connection to close'); resolve()}
		})
	}
	/**custom query
	 *
	 * allows for custom SQL string expression for complex queries
	 *
	 * @param query			:custom query string
	 * @param params		:optional query parameters for query
	 *
	 * @example
	 * facade.query('SELECT * FROM TABLE WHERE i = 1).then((data) => {console.log(data)}).catch((err) => {new Error(err.message)})
	 *
	 * @returns : Promise<ResultRow[]>
	 */
	query(query: string, params?: object): Promise<WriteOpsResults<{}>> {
		return new Promise(async (resolve, reject) => {
			if (this._db) {
				this._db.query(query, params ? params : {})
				.then(async (cursor: ArrayCursor) => {
					return {stats: {
						error: false
						, created: 0
						, errors: cursor.extra.warnings.length
						, updated: cursor.extra.stats.writesExecuted
						, ignored: cursor.extra.stats.writesIgnored
					} , data: await cursor.all()}
				})
				.then((data: WriteOpsResults<{}>) => {
					resolve(data)
				})
				.catch((err: ArangoError) => {this._log.error(new Date(), ' : ', err); reject(err)})
			} else {log.error(new Date(), ' : Database not connected'); reject('Database not connected')}
		})
	}
}

export default ArangoDatabaseStorage







